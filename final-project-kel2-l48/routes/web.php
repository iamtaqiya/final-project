<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\ProdukController;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Mengatur view welcome menjadi default ketika pertama kali membuka web
Route::get('/', function () {
    return view('welcome');
});

// Kami hanya membuat navigasi pagenya saja oleh karena itu kami return agar page about dapat ditampilkan
Route::get('/about', function () {
    return view('about');
});

// Kami hanya membuat navigasi pagenya saja oleh karena itu kami return agar page news dapat ditampilkan
Route::get('/news', function () {
    return view('news');
});

// Kami hanya membuat navigasi pagenya saja oleh karena itu kami return agar page contact dapat ditampilkan
Route::get('/contact', function () {
    return view('contact');
});

// Kami hanya membuat navigasi pagenya saja oleh karena itu kami return agar page shop dapat ditampilkan
Route::get('/shop', function () {
    return view('shop');
});

// Kami hanya membuat navigasi pagenya saja oleh karena itu kami return agar page checkout dapat ditampilkan
Route::get('/checkout', function () {
    return view('checkout');
});

// Kami hanya membuat navigasi pagenya saja oleh karena itu kami return agar page single product dapat ditampilkan
Route::get('/single-product', function () {
    return view('single-product');
});

// Kami hanya membuat navigasi pagenya saja oleh karena itu kami return agar page single news dapat ditampilkan
Route::get('/single-news', function () {
    return view('single-news');
});

//CRUD produk
// Create
Route::get('/home/product/create', [ProductController::class, 'create']);
Route::post('/home/product/create', [ProductController::class, 'store']);
// Read
Route::get('/home/product/{product:id}', [ProductController::class, 'show']);
// Update
Route::get('/home/product/{product:id}/edit', [ProductController::class, 'edit']);
Route::put('/home/product/{product:id}/update', [ProductController::class, 'update']);
// Delete
Route::delete('/home/product/{product:id}/delete', [ProductController::class, 'destroy']);

//CRUD kategori
// Create
Route::get('/kategori/create', [ProductController::class, 'create']);
Route::post('/kategori/create', [ProductController::class, 'store']);
// Read
Route::get('/kategori/{kategori:id}', [ProductController::class, 'show']);
// Update
Route::get('/kategori/{kategori:id}/edit', [ProductController::class, 'edit']);
Route::put('/kategori/{kategori:id}/update', [ProductController::class, 'update']);
// Delete
Route::delete('/kategori/{kategori:id}/delete', [ProductController::class, 'destroy']);

//CRUD keranjang
// Create
Route::post('/cart/create', [CartController::class, 'store']);
// Read
Route::get('/cart', [CartController::class, 'index']);
// Update
Route::put('/cart/{cartDetail:id}/update', [CartController::class, 'update']);
// Delete
Route::delete('/cart/{cartDetail:id}/delete', [CartController::class, 'destroy']);


//setelah php artisan ui bootstrap --auth
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
