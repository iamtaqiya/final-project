<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Kategori;
use App\Models\Produk;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $isLoggedIn = Auth::check();
        if($isLoggedIn == false){
            return redirect('/login');
        } else {
            return redirect('/home');
        } 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()){

            $validatedData = $request->validate([ //validasi input
                'nama' =>'required'
            ]);

            Kategori::create($validatedData);
            return redirect('/home'); //redirect ke home untuk melihat list kategori
        } else {
            return redirect('/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Kategori $kategori)
    {
        return view('Kategori', [
            'isLoggedIn' => Auth::check(),
            'user' => Auth::user(),
            'Kategori' => $kategori,
            'Products' => Produk::all()->where('kategori_id', '=' , $kategori->id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kategori $kategori)
    {
        $isLoggedIn = Auth::check();
        if ($isLoggedIn){
            return view('EditGudang', [
                'isLoggedIn' => $isLoggedIn,
                'Kategori' => $kategori,
                'Products' => Produk::all()->where('kategori_id', '=' , $kategori->id)
            ]);
        }else {
            return redirect('/home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kategori $kategori)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
        ]);
        Kategori::where('id', $kategori->id)->update($validatedData);
        return redirect('/home/kategori/'.$kategori->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kategori $kategori)
    {
        Kategori::destroy($kategori->id);
        Produk::where('kategori_id', $kategori->id)->delete();
        return redirect('/home');
    }
}
