<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $isLoggedIn = Auth::check();
        
        if($isLoggedIn == false){ //jika user belum login, maka akan diredirect ke halaman login
            return redirect()->intended('/login');
        }
        $user = Auth::user();
        
        if ($isLoggedIn) {
            return view('Profil', [
                'isLoggedIn' => $isLoggedIn,
                'user' => $user
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validatedData = $request->validate([ //validasi inputan
            'nama_lengkap' => 'required',
            'nomor_hp' => 'required|min:10|max:15',
            'alamat' => 'required|min:15|max:99',
            'email' => 'required|email:dns',
            'password' => 'required|min:6|max:50',
        ]);

        if ($validatedData['password'] != $request['confirm_password']) //validasi password
        {
            return back();
        }

        $validatedData['password'] = bcrypt($validatedData['password']); //enkripsi password

        User::where('id', $user->id)->update($validatedData); //update user nya
        return redirect('/home/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        User::destroy($user->id);
        return redirect('/login');
    }
}
