<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Cart;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        if(Auth::check()){
            $user = Auth::user();
            $TotalHarga = 0;
            $SelectedProducts = Cart::all()->where('user_id', Auth::id());
            foreach ($SelectedProducts as $SelectedProduct) {
                $TotalHarga += $SelectedProduct->jumlah * $SelectedProduct->produk->price;
            }
            return view('cart', [
                'isLoggedIn' => Auth::check(),
                'user' => $user,
                'SelectedProducts' => $SelectedProducts,
                'TotalHarga' => $TotalHarga
            ]);
        } else {
            return redirect('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()){
            $validatedData = $request->validate([ //cek apakah inputannya valid 
                'user_id' =>'required',
                'produk_id' => 'required',
                'jumlah' => 'required'
            ]);

            if (count(Cart::where('user_id', $validatedData['user_id'])->where('produk_id', $validatedData['produk_id'])->get()) == 0) { //memastikan bahwa produk tersebut belum ada di cart sehingga tidak ada produk yang duplikat di cart.
                Cart::create($validatedData); //bikin entry data di database
            }

            if ($request->redirect_cart == 1) { //mengecek apakah user menekan tombol keranjang atau tombol order, jika user menekan tombol order akan diredirect ke keranjang secara otomatis.
                return redirect('/cart'); //redirect ke keranjang
            } else { //mengecek apakah user menekan tombol keranjang atau tombol order, jika user menekan tombol keranjang, maka produk akan ditambahkan ke keranjang dan user tetap berada di halaman produk.
                return back(); //user tetap berada di halaman produk
            }
            
        } else {
            return redirect('/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        $validatedData = $request->validate([
            'jumlah' => 'required',
        ]);
        if ($validatedData['jumlah'] <= 0) { //jika jumlah <= 0 maka produk akan dihapus dari keranjang
            Cart::destroy($cart->id);
            return redirect('/cart');
        } else { //Jika jumlah > 0 maka jumlah dari produk yang ada di keranjang akan di update.
            Cart::where('id', $cart->id)->update($validatedData);
            return redirect('/cart');  
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        Cart::destroy($cart->id);
        return back();
    }
}
