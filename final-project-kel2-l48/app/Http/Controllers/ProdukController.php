<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Models\Produk;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $isLoggedIn = Auth::check();
        if($isLoggedIn == false){
            return redirect('/login');
        }
        else {
            $validatedData = $request->validate([
                'kategori_id' =>'required'
            ]);
            return view('TambahProduk', [
                'isLoggedIn' => $isLoggedIn,
                'Kategori_id' => $validatedData['kategori_id']
            ]);
        } 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()){
            $validatedData = $request->validate([
                'kategori_id' =>'required',
                'nama' =>'required',
                'deskripsi' =>'required',
                'harga' =>'required',
                'gambar' =>'required',
                'stok' =>'required'
            ]);
            Produk::create($validatedData);
            return redirect('/home'); //redirect ke home
        } else {
            return redirect('/home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Produk $produk)
    {
        if(Auth::check()){
            return view('Produk', [
                'isLoggedIn' => Auth::check(),
                'user' => Auth::user(),
                'Produk' => Produk::where('id' , $produk->id)->get()
            ]);
        } else {
            return redirect('/login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Produk $produk)
    {
        if(Auth::check()){
            return view('EditProduk', [
                'isLoggedIn' => Auth::check(),
                'user' => Auth::user(),
                'Produk' => $produk
            ]);
        } else {
            return redirect('/home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produk $produk)
    {
        if (Auth::check()){
            $validatedData = $request->validate([
                'kategori_id' =>'required',
                'nama' =>'required',
                'deskripsi' =>'required',
                'harga' =>'required',
                'gambar' =>'required',
                'stok' =>'required'
            ]);
            $request->validate(['gambar' =>'required']);
            $path = $request->file('gambar')->store('public/images/products');
            $url = Storage::url($path);
            $validatedData['gambar_link'] = $url;
            Produk::where('id', $produk->id)->update($validatedData);
            return redirect('/home/produk/'.$produk->id);
        } else {
            return redirect('/home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produk $produk)
    {
        if(Auth::check()){
            $kategori_id = $produk->kategori_id;
            Produk::destroy($produk->id);
            return redirect('/home/kategori/'.$kategori_id);
        } else {
            return redirect('/home');
        }
    }
}
