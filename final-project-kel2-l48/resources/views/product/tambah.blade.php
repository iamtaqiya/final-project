<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
  <title>Document</title>
</head>
<body style="width:300px;margin-left:50px;margin-top:20px;">
  <h1>Tambah Cast</h1>

  <form method="post" action="/cast">
      @csrf
    <div class="form-group">
      <label>Nama Cast</label>
      <input name="nama" class="form-control @error('nama') is-invalid @enderror">
    </div>
    @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>
    <div class="form-group">
      <label>Umur</label>
      <input name="umur" type="number" class="form-control @error('umur') is-invalid @enderror">
    </div>
    @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>
    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" cols="30" rows="10" class="form-control @error('bio') is-invalid @enderror"></textarea>
    </div>
    @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
</body>
</html>