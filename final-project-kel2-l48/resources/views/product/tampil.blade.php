<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
  <title>Document</title>
</head>
<body>
  <h1 style="margin-left: 30px;">Tampil Cast</h1>

  <a href="/cast/create" class="btn btn-primary" style="margin-bottom: 10px;margin-left: 30px;">Tambah Cast</a>

  <table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $item)
          <tr>
          <th scope="row">{{ $key + 1 }}</th>
          <td>{{ $item->nama }}</td>
          <td>{{ $item->umur }}</td>
          <td>{{ $item->bio }}</td>
          <td>
            <form action="/cast/{{$item->id}}" method="post">
              @csrf
              @method('delete')
              <a href="/cast/{{$item->id}}" class="btn btn-primary">Detail</a>
              <a href="/cast/{{$item->id}}/edit" class="btn btn-success" style="margin-right: 10px;margin-left: 10px;">Edit</a>
              <input type="submit" value="Delete" class="btn btn-danger">
            </form>
          </td>
          </tr>
      @empty
        <tr>
          <td> Tidak Ada Cast </td>
        </tr>
      @endforelse
    </tbody>
  </table>
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
</body>
</html>