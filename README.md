# Final Project

## Kelompok 2

## Anggota Kelompok
- Balqis Taqiya Putri (@balqistp)
- Dea Yova Septri Saragih (@deayovaseptrisaragih)
- Muhammad Raihan Fauzi (@mraihanfauzi)

## Tema Project
E-commerce

## ERD
![ERD](desain-erd/Final Project ERD.png)

## Link Video
[Link Demo Aplikasi](https://youtu.be/FavQ4e6KUZk)
